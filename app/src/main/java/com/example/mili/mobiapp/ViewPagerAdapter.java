package com.example.mili.mobiapp;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ViewPagerAdapter extends PagerAdapter {
    private int[] image = {R.drawable.h3,R.drawable.h2,R.drawable.h4};
    private LayoutInflater inflater;
    private Context context1;

    public ViewPagerAdapter( LayoutInflater inflater, Context context1) {

        this.inflater = inflater;
        this.context1 = context1;
    }

    public ViewPagerAdapter( Context context1) {
        this.context1 = context1;
    }

    @Override
    public int getCount() {
        return image.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        inflater = (LayoutInflater)context1.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item_viewpager,container,false);
        ImageView display = view.findViewById(R.id.image_viewpager);
        display.setImageResource(image[position]);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);

    }

}
