package com.example.mili.mobiapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class CustomListAdapter extends BaseAdapter {

    private List<ItemListview>listData;
    private LayoutInflater layoutInflater;
    private Context context;

    public CustomListAdapter(List<ItemListview> listData, LayoutInflater layoutInflater, Context context) {
        this.listData = listData;
        this.layoutInflater = layoutInflater;
        this.context = context;
    }

    public CustomListAdapter(List<ItemListview> listData, Context context) {
        this.listData = listData;
        this.context = context;
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int i) {
        return listData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        ViewHolder holder;
        if(view == null){
            view = layoutInflater.from(context).inflate(R.layout.item_listview,null);
            holder = new ViewHolder();
            holder.imageView = view.findViewById(R.id.imageView_flag);
            holder.txtTiltle = view.findViewById(R.id.txt_tiltle);
            holder.txtContent = view.findViewById(R.id.txt_content);
            view.setTag(holder);
        }else{
            holder = (ViewHolder) view.getTag();
        }
        ItemListview itemListview = this.listData.get(i);
        holder.txtTiltle.setText(itemListview.getM_tiltle());
        holder.txtContent.setText(itemListview.getM_content());

        int imageId = this.getMipMap(itemListview.getM_image());
        holder.imageView.setImageResource(imageId);
        return view;
    }

    public int getMipMap(String resName){
        String pkgName = context.getPackageName();

        int resID = context.getResources().getIdentifier(resName,"mipmap",pkgName);
        return  resID;
    }

    static  class ViewHolder{
        ImageView imageView;
        TextView txtTiltle;
        TextView txtContent;
    }
}
