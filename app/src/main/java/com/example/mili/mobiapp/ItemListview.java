package com.example.mili.mobiapp;

public class ItemListview {

    private String m_image;
    private String m_tiltle;
    private String m_content;

    public ItemListview(String m_image, String m_tiltle, String m_content) {
        this.m_image = m_image;
        this.m_tiltle = m_tiltle;
        this.m_content = m_content;
    }


    public String getM_image() {
        return m_image;
    }

    public void setM_image(String m_image) {
        this.m_image = m_image;
    }

    public String getM_tiltle() {
        return m_tiltle;
    }

    public void setM_tiltle(String m_tiltle) {
        this.m_tiltle = m_tiltle;
    }

    public String getM_content() {
        return m_content;
    }

    public void setM_content(String m_content) {
        this.m_content = m_content;
    }

    @Override
    public String toString() {
        return "ItemListview{" +
                "m_image='" + m_image + '\'' +
                ", m_tiltle='" + m_tiltle + '\'' +
                ", m_content='" + m_content + '\'' +
                '}';
    }
}
