package com.example.mili.mobiapp;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    FrameLayout _baseLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Window w = getWindow();
        w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        _baseLayout = findViewById(R.id.baseLayout);

        LoginFragment loginFragment = new LoginFragment();
        Log.d("ERRor","Error");
        pushFragment(loginFragment);
    }

    public void pushFragment(Fragment fragment){
        _baseLayout.removeAllViews();
        processPushFragment(R.id.baseLayout, fragment);
    }

    public void processPushFragment(@IdRes final int idLayout, final Fragment fragment) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Context mainActivity = getBaseContext();
                if (fragment != null) {
                    if(getSupportFragmentManager().getFragments()!=null && getSupportFragmentManager().getFragments().size()>=0){
                       // Toast.makeText(mainActivity,"Hiện Tại:"+getSupportFragmentManager().getFragments().size(),Toast.LENGTH_SHORT).show();
                    }
                    if (getSupportFragmentManager().getFragments() != null && getSupportFragmentManager().getFragments().size() > 0) {
                        int check = -1;
                        int size = getSupportFragmentManager().getFragments().size();
                        for (int i = 0; i < size; i++) {
                            if (getSupportFragmentManager().getFragments().get(i) != null) {
                                Fragment temp = getSupportFragmentManager().getFragments().get(i);
                                Log.e("-->", temp.getClass().toString());
                                Log.e("-->", fragment.getClass().toString());
                                if (getSupportFragmentManager().getFragments().get(i).getClass().toString().equalsIgnoreCase(fragment.getClass().toString())) {
                                    check = i;
                                    break;
                                }
                            }
                        }
                        if (check >= 0) {//Đã tồn tại
                            if(check == 0){
                                getSupportFragmentManager().beginTransaction().add(idLayout, fragment,
                                        fragment.getClass().getSimpleName()).addToBackStack(null).commit();
                                getSupportFragmentManager().executePendingTransactions();
                                //Toast.makeText(mainActivity,"Khởi tạo!",Toast.LENGTH_SHORT).show();
                            }else {
                                int backStackId = getSupportFragmentManager().getBackStackEntryAt(check - 1).getId();
                                getSupportFragmentManager().popBackStack(backStackId + 1, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                getSupportFragmentManager().executePendingTransactions();
                                //Toast.makeText(mainActivity,"Đã tồn tại vị trí "+check,Toast.LENGTH_SHORT).show();
                            }
                        } else {//Chưa tồn tại
                            getSupportFragmentManager().beginTransaction().add(idLayout, fragment,
                                    fragment.getClass().getSimpleName()).addToBackStack(null).commit();
                            getSupportFragmentManager().executePendingTransactions();
                           // Toast.makeText(mainActivity,"Chưa tồn tại thêm mới!",Toast.LENGTH_SHORT).show();
                        }
                    } else {//khởi tạo
                        getSupportFragmentManager().beginTransaction().add(idLayout, fragment,
                                fragment.getClass().getSimpleName()).addToBackStack(null).commit();
                        getSupportFragmentManager().executePendingTransactions();
                        //Toast.makeText(mainActivity,"Khởi tạo!",Toast.LENGTH_SHORT).show();
                    }
                    if(getSupportFragmentManager().getFragments()!=null && getSupportFragmentManager().getFragments().size()>=0){
                       // Toast.makeText(mainActivity,"Sau Đó:"+getSupportFragmentManager().getFragments().size(),Toast.LENGTH_SHORT).show();
                    }
                }
                fragment.onResume();
            }
        });
    }
}
