package com.example.mili.mobiapp;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;


public class HomeFragment extends Fragment {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        List<ItemListview> itemListviews = getListData();
        final ListView listView = view.findViewById(R.id.lst);
        final ViewPager viewPager = view.findViewById(R.id.imageView_flag);
        ViewPagerAdapter viewPagerAdapter;
        listView.setAdapter(new CustomListAdapter(itemListviews,getActivity()));
        viewPager.setAdapter(new ViewPagerAdapter(getActivity()));
    }

    private  List<ItemListview>getListData(){
        List<ItemListview>list = new ArrayList<ItemListview>();
        ItemListview listview1 = new ItemListview("facebook","TOITENLA","lllll");
        ItemListview listview2 = new ItemListview("zalo","TOITENLA","lllll");
        ItemListview listview3 = new ItemListview("facebook","TOITENLA","lllll");
        ItemListview listview4 = new ItemListview("zalo","TOITENLA","lllll");

        list.add(listview1);
        list.add(listview2);
        list.add(listview3);
        list.add(listview4);
        return list;
    }

}
